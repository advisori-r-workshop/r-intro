---
title: "Workshop: R Intro"
output:
  rmdformats::readthedown:
    highlight: pygments
    css: media/style.css
    df_print: paged
    toc_depth: 3
---

# Cleaning messy data


```{r  message=FALSE, warning=FALSE}
library(tidyverse)
source("00_Setup.R")
source("Solutions.R")
```

## Make you import explicit

![](media/hex-readr.png){width=80px}

We already used `readr` to import csv data. It's strength is the flexibility and transparency when parsing data.

```{r}

tibble(id = seq(3),
       double = c(1, 2.5, "ops"),
       date = c("2020-01-01", "2020-20-20", "")) %>% 
  readr::write_csv("data/readr_test.csv")

test_data <- readr::read_csv("data/readr_test.csv")
test_data
```

Is always a good idea to make your assumptions explicit:

```{r error=TRUE}
suppressWarnings({
test_data <- readr::read_csv("data/readr_test.csv",
                             col_types = cols(
                               id = col_double(),
                               double = col_double(),
                               date = col_date(format = "")
                             ))
})
test_data
problems_df <- problems(test_data)
problems_df
# Throw an error on problems. Useful for use in a script
stop_for_problems(test_data)
```




## Handle dates with lubridate

![](media/hex-lubridate.png){width=80px}

```{r}
library(lubridate)
```


**Beware:** Dates are ugly!

* timezones
* daily light savings time
* months calculus
* calendar standards
* leap year bugs [^Leap_Year_1900]
* leap seconds

#### Three different types: date, datetime and time

```{r}
my_date <- lubridate::ymd("2019-12-31") # default: tz=NULL 
my_datetime <- lubridate::ymd("2019-12-31", tz="CET")
my_time <- hms::hms(30, 10)

my_times <- list(my_date, my_datetime, my_time)


lapply(my_times, class)
lapply(my_times, as.numeric)

```


#### Flexible dates parsing:

```{r}
x <- list(20090101, "2009-01-02", "2009 01 03",
          "2009, March 4", "2009-1, 5", 
          "Created on 2009 1 6", "200901 !!! 07")
ymd(x)
# see also the order variants: ydm, dym, myd, dmy, mdy
# or more flexible: parse_date_time
```

```{r}
ymd(0302, truncated = 1)
yq(0302)
```

#### Computing with dates

```{r}

my_date + 5*days()
my_date %m+% months(1:4)

my_date %m+% months(2) %m+% years(1:4)
# or use add_with_rollback for more options
```

```{r}
# But
ymd("2020-02-28") %m+% years(1:4)
ymd("2020-02-28") %m+% years(1:4) %>% ceiling_date("weeks", week_start=1) %>% format("%a, %Y-%m-%d")
```


## Manipulate text with stringr and glue

![](media/hex-glue.png){width=80px}

```{r}
library(glue)
```

**Remark:** [^Warning_Masked]

Replace ugly `paste` statements with `glue`

```{r}
head(iris) %>%
  mutate(description = glue("This {Species} has a petal length of {Petal.Length}"))
```


![](media/hex-stringr.png){width=80px}


```{r}
library(stringr)
```

Detect or count the appearence of a regular expression

```{r}
fruit <- c("apples", "pear", "pinapple", "banana")
str_length(fruit)

pattern <- ".?(.a)"
str_detect(fruit, pattern)
str_count(fruit, pattern)
```

Extract complete or sub-group matches

```{r}
str_extract_all(fruit, pattern)
str_match_all(fruit, pattern)
```

Modify the strings

```{r}
str_replace_all(fruit, pattern, "X")
str_pad(fruit, width=7, pad="_")
str_sort(fruit)
```



## Data tidying with tidyr

![](media/hex-tidyr.png){width=80px}

> **Question:**  
Is this tidy data? Whats wrong with it and how would you correct it?

```{r}
set.seed(42)
trials <- tibble(
  trial_date = as.Date('2020-01-01') + 0:9,
  Subject_A = rnorm(10, 1, 1),
  Subject_B = rnorm(10, 1, 2),
  Subject_C = rnorm(10, 1, 4),
  Control_A = rnorm(10, 0, 1),
  Control_B = rnorm(10, 0, 2),
  Control_C = rnorm(10, 0, 4)
)
trials 
```

### Reshape data

#### Pivot

First, we turn the subject variables into observations:

```{r}
## pivot_longer: turn variables (columns) into observations (rows)
trials_tidy_1 <- trials %>% 
  tidyr::pivot_longer(-trial_date, 
                      names_to = "subject", 
                      values_to = "measurement")
trials_tidy_1
```

```{r}
## pivot_wider: turn observations (rows) into variables (columns)
# inverse of pivot_longer
trials_tidy_1 %>% 
  tidyr::pivot_wider(trial_date,
                     names_from = subject, 
                     values_from = measurement)
```

Then, we split each subject information into two variables:

```{r}
## separate: separate one column into multiple columns 
trials_tidy_2 <- trials_tidy_1 %>%
  tidyr::separate(subject, into=c("group", "person"), sep="_")
trials_tidy_2
```

**Remark:** [^Tidyr_Extract]


```{r}
## unite: unite multiple columns into one
# inverse of separate
trials_tidy_2 %>% 
  unite("subject", group, person, sep="::")
```

> **Try it:**  
Now, given the tidy data `trials_tidy_2`, can you calculate the trial impact (which is difference between groups 'Subject' and 'Control') per person?

```{r class.source="exercise"}
#
```


```{r class.source="solution"}
# %show_solution=ex_tidyr_trials_reshape
```

